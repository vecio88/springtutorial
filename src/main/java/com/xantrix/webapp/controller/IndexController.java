package com.xantrix.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller // Indica al dispatcher che questa � la classe controller
public class IndexController {
	
	@RequestMapping(value="/index") // tutte le chiamate index vengono gestite tramite questa annotazione
	public String getWelcome(Model model) {
		model.addAttribute("intestazione", "Benvenuti nel sito Alpha Shop INDEX");
		model.addAttribute("saluti","Seleziona i prodotti da acquistare");
		
		return "index";
	}
	
	@RequestMapping(value="/") // tutte le chiamate / vengono gestite tramite questa annotazione
	public String getWelcome2(Model model) {
		model.addAttribute("intestazione", "Benvenuti nel sito Alpha Shop / ");
		model.addAttribute("saluti","Seleziona i prodotti da acquistare");
		
		return "index";
	}
}
