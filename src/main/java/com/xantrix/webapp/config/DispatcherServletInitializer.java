package com.xantrix.webapp.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class DispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		/*
		 * Qui specificheremo la classe pi� importante della nostra app, 
		 * ovvero la classe di configurazione 
		 */
		return new Class[] {
				WebApplicationContextConfig.class
		};
	}

	@Override
	protected String[] getServletMappings() {
		/*
		 * Nel servletMapping viene indicato come gestire le chiamate
		 * in questo modo indichiamo al dispatcher che ogni chiamata 
		 * deve essere gestita dal DispatcherServelet
		 */
		return new String[] {"/"}; 
	}

}
