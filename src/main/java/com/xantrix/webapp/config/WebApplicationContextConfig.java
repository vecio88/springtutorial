package com.xantrix.webapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/*
 * Questa classe gestira le configurazioni dell'applicazione
 */
@Configuration // Indica che questa � una classe di configurazione
@EnableWebMvc // Abilitiamo le abilita dello spring MVC
@ComponentScan(basePackages = "com.xantrix.webapp") // Indica dove andare a cercare il controller 
public class WebApplicationContextConfig implements WebMvcConfigurer {
	
	/*
	 * Le annotazioni indicano le classi cosa sono
	 */
	
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
	
	// Configuriamo il bean che ci permette di creare il view resolver 
	@Bean
	public InternalResourceViewResolver getInternalResourceViewResolver() {
		
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setViewClass(JstlView.class);
		// Stiamo indicando che deve cercare i file JSP a questo percorso con il suffisso jsp
		resolver.setPrefix("/WEB-INF/view/");
		resolver.setSuffix(".jsp");
		
		return resolver;
	}
	

}
